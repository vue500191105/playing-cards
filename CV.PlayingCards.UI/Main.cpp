#include <iostream>
#include <conio.h>
#include <string>

//Chan Vue
//500191105

using namespace std;

enum suit{
	CLOVER,
	DIAMOND,
	HEART,
	SPADES
};

enum rank {
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

struct Card {
	int rank;
	string suit;
};